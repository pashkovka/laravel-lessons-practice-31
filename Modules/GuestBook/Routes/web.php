<?php


Route::prefix('guestbook')->group(function() {

    Route::get('/', 'PostsController@index');
    Route::post('/post/add', 'PostsController@store');

    Route::prefix('admin')->group(function (){

        Route::get('/', 'AdminPostsController@index')->name('guestbook.post.all');
        Route::get('/edit/{id}', 'AdminPostsController@edit')->name('guestbook.post.edit');
        Route::get('/destroy/{id}', 'AdminPostsController@destroy')->name('guestbook.post.destroy');
        Route::post('/update/{id}', 'AdminPostsController@update')->name('guestbook.post.update');
    });
});
