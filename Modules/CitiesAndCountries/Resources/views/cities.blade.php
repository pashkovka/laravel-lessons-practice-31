@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Города</h1>

    @foreach($cities as $city)
        <div class="card mb-2">
            <div class="card-title">
                <h3><a href="{{ route('places', $city->id) }}">{{ $city->name }}</a></h3>
            </div>
        </div>
    @endforeach
@endsection