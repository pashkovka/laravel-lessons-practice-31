<?php

use Faker\Generator as Faker;

$factory->define(\Modules\BulletinBoard\Entities\BulletinBoardCategory::class, function (Faker $faker) {
    $time = $faker->dateTime('now', 'Europe/Moscow');
    return [
        'name'=>$faker->unique()->words(2)[0],
        'created_at'=>$time,
        'updated_at'=>$time
    ];
});
