<?php

namespace Modules\CitiesAndCountries\Entities;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];


    public function places()
    {
        return $this->hasManyThrough(ShowPlace::class, City::class);
    }


    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
