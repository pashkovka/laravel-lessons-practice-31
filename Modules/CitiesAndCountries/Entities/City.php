<?php

namespace Modules\CitiesAndCountries\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name', 'country_id'];


    public function places()
    {
        return $this->hasMany(ShowPlace::class);
    }
}
