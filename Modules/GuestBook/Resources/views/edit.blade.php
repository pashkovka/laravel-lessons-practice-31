@extends('guestbook::layouts.master')

@section('content')
    <div class="container">
        <form action="{{ route('guestbook.post.update', $post->id) }}" method="post">
            @csrf
            <h3>Заголовок</h3>
            <p>
                <input type="text" name="title" value="{{ $post->title }}" class="form-control">
            </p>
            <h3>Сообщение</h3>
            <p>
                <textarea name="post" cols="30" rows="10" class="form-control">{{ $post->post }}</textarea>
            </p>
            <input type="submit" name="self" value="Обновить"> &nbsp;
            <input type="submit" name="all" value="Обновить и выйти на список постов">
        </form>
    </div>

@endsection