<?php

namespace Modules\CitiesAndCountries\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\CitiesAndCountries\Entities\ShowPlace;

class ShowPlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(ShowPlace::class, 2000)->create();
    }
}
