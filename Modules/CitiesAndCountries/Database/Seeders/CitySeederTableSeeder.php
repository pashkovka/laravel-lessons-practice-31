<?php

namespace Modules\CitiesAndCountries\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\CitiesAndCountries\Entities\City;

class CitySeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(City::class, 300)->create();
    }
}
