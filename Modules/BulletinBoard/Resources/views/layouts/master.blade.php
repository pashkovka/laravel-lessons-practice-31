<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module BulletinBoard</title>

       {{-- Laravel Mix - CSS File --}}
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <style>
            form{
                margin-bottom: 50px;
            }
        </style>

    </head>
    <body>

    @include('menu_modules')

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
