<?php

namespace Modules\CitiesAndCountries\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\CitiesAndCountries\Entities\Country;

class CountriesSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(Country::class, 20)->create();
    }
}
