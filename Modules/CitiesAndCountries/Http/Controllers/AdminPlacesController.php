<?php

namespace Modules\CitiesAndCountries\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CitiesAndCountries\Entities\City;
use Modules\CitiesAndCountries\Entities\ShowPlace;

class AdminPlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('citiesandcountries::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        $city = City::findOrFail($id);
        return view('citiesandcountries::admin.place_create', compact('city'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        ShowPlace::create([
          'name' => $request->name,
          'description' => $request->description,
          'city_id' => $request->city_id
        ]);
//todo route
        return redirect()->route('admin.place.create', $request->city_id);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('citiesandcountries::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $place = ShowPlace::findOrFail($id);
        return view('citiesandcountries::admin.place_edit', compact('place'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        \DB::table('show_places')
          ->where('id', $id)
          ->update([
            'name' => $request->name,
            'description' => $request->description
          ]);
        session()->flash('success', 'Обновлено');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::table('show_places')
          ->where('id', $id)
          ->delete();
        session()->flash('success', 'Удалено');

        return back();
    }
}
