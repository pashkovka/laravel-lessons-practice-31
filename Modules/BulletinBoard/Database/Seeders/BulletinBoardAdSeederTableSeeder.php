<?php

namespace Modules\BulletinBoard\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\BulletinBoard\Entities\BulletinBoardAd;

class BulletinBoardAdSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(BulletinBoardAd::class, 100)->create();
    }
}
