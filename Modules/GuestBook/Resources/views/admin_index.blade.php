@extends('guestbook::layouts.master')

@section('content')
    <table class="table table-bordered table-hover">
        <tr>
            <th>ID</th>
            <th>TITLE</th>
            <th>POST</th>
            <th>USER NAME</th>
            <th>USER EMAIL</th>
            <th>#</th>
            <th>#</th>
        </tr>
        @foreach($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->post }}</td>
                <td>{{ $post->user->name }}</td>
                <td>{{ $post->user->email }}</td>
                <td><a href="{{ route('guestbook.post.edit', $post->id) }}">Редактировать</a></td>
                <td><a href="{{ route('guestbook.post.destroy', $post->id) }}">Удалить</a></td>
            </tr>
        @endforeach
    </table>
@endsection