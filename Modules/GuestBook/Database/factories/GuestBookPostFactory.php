<?php

use Faker\Generator as Faker;

$factory->define(\Modules\GuestBook\Entities\GuestBookPost::class, function (Faker $faker) {
    $users = \App\User::all(['id'])->toArray();
    $ids = [];
    foreach ($users as $user){
        $ids[] = $user['id'];
    }

    return [
        'user_id' => $ids[array_rand($ids)],
        'title'=>$faker->sentence(),
        'post'=>$faker->paragraph(5)
    ];
});
