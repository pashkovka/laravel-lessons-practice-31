@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Страна</h1>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.country.update', $country->id) }}" method="post">
                @csrf
                <p><input type="text" value="{{ $country->name }}" name="name"></p>
                <p><input type="submit" value="Сохранить"></p>
            </form>
        </div>
    </div>
@stop