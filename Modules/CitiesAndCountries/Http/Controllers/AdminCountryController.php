<?php

namespace Modules\CitiesAndCountries\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CitiesAndCountries\Entities\City;
use Modules\CitiesAndCountries\Entities\Country;

class AdminCountryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $countries = Country::all();
        return view('citiesandcountries::admin.index', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('citiesandcountries::admin.country_create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        Country::create([
          'name' => $request->name,
        ]);

        return redirect()->route('admin.country.list');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $cities = City::all()->where('country_id', $id);
        return view('citiesandcountries::admin.cities_show', compact('cities'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('citiesandcountries::admin.country_edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        \DB::table('countries')
          ->where('id', $id)
          ->update([
            'name' => $request->name,
          ]);
        session()->flash('success', 'Обновлено');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);

        $country->places()->delete();
        $country->cities()->delete();
        $country->delete();

        session()->flash('success', 'Удалено');

        return back();
    }
}
