@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Достопримечательности</h1>
    <a href="{{ route('admin.place.create', $id) }}">Добавить достопримечательность</a>
    <table class="table table-hover">
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>CREATED</th>
            <th>UPDATED</th>
            <th>#</th>
            <th>#</th>
        </tr>
        @foreach($places as $place)
            <tr>
                <td>{{ $place->id }}</td>
                <td>{{ $place->name }}</td>
                <td>{{ $place->created_at }}</td>
                <td>{{ $place->updated_at }}</td>
                {{--todo: --}}
                <td><a href="{{ route('admin.place.edit', $place->id) }}">Редактировать</a></td>
                <td><a href="{{ route('admin.place.destroy', $place->id) }}" onclick="return confirm('Удалить?')">Удалить</a></td>
            </tr>
        @endforeach
    </table>
@stop