<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module CitiesAndCountries</title>

       {{-- Laravel Mix - CSS File --}}
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    </head>
    <body>

    @include('menu_modules')

        <div class="container">
            <a href="{{ route('countries') }}">Страны</a>
            <a href="{{ route('admin.country.list') }}">Админка: Страны</a>
        </div>
        <div class="container">
            @if(Session::has('success'))
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <p style="color: green" class="text-center h3">{{ Session::get('success') }}</p>
                        </div>
                    </div>
                </div>
            @endif

            @yield('content')
        </div>
    </body>
</html>
