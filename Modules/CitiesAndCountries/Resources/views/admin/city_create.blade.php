@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Добавить город</h1>
    <div class="card">
        <div class="card-title">
            <p>Страна: <b>{{ $country->name }}</b></p>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.city.store') }}" method="post">
                @csrf
                <input type="hidden" name="country_id" value="{{ $country->id }}">
                <p><input type="text" name="name" size="150"></p>
                <p><input type="submit" value="Сохранить"></p>
            </form>
        </div>
    </div>
@stop