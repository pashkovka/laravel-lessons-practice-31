<?php

namespace Modules\BulletinBoard\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\BulletinBoard\Entities\BulletinBoardCategory;

class BulletinBoardCategoriesSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(BulletinBoardCategory::class, 10)->create();
    }
}
