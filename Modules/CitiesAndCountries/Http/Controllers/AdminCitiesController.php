<?php

namespace Modules\CitiesAndCountries\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CitiesAndCountries\Entities\City;
use Modules\CitiesAndCountries\Entities\Country;
use Modules\CitiesAndCountries\Entities\ShowPlace;

class AdminCitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('citiesandcountries::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        $country = Country::findOrFail($id, ['name', 'id']);
        return view('citiesandcountries::admin.city_create', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $city = City::create([
          'name' => $request->name,
          'country_id' => $request->country_id
        ]);

        return redirect()->route('admin.cities.edit', $request->country_id);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $places = ShowPlace::all()->where('city_id', $id);
        return view('citiesandcountries::admin.places_show', compact('places', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view('citiesandcountries::admin.city_edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        \DB::table('cities')
          ->where('id', $id)
          ->update([
            'name' => $request->name
          ]);
        session()->flash('success', 'Обновлено');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->places()->delete();
        $city->delete();
        session()->flash('success', 'Удалено');

        return back();
    }
}
