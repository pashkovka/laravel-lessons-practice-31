<?php

namespace Modules\GuestBook\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GuestBookPost extends Model
{
    protected $fillable = ['title','post', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
