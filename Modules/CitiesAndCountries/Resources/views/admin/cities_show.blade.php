@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Города</h1>
    <table class="table table-hover">
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>CREATED</th>
            <th>UPDATED</th>
            <th>#</th>
            <th>#</th>
            <th>#</th>
        </tr>
        @foreach($cities as $city)
            <tr>
                <td>{{ $city->id }}</td>
                <td><a href="{{ route('admin.places.show', $city->id) }}">{{ $city->name }}</a></td>
                <td>{{ $city->created_at }}</td>
                <td>{{ $city->updated_at }}</td>
                {{--todo route--}}
                <td><a href="{{ route('admin.place.create', $city->id) }}">Добавить достоп.</a></td>
                <td><a href="{{ route('admin.city.edit', $city->id) }}">Редактировать</a></td>
                <td><a href="{{ route('admin.city.destroy', $city->id) }}" onclick="return confirm('Удалить все вложенные элементы?')">Удалить</a></td>
            </tr>
        @endforeach
    </table>
@stop