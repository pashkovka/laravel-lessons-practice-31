@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Страны</h1>

    @foreach($countries as $country)
        <div class="card mb-2">
            <div class="card-title">
                <h3><a href="{{ route('city', $country->id) }}">{{ $country->name }}</a></h3>
            </div>
        </div>
    @endforeach
@stop
