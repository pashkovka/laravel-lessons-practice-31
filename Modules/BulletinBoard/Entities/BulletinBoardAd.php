<?php

namespace Modules\BulletinBoard\Entities;

use Illuminate\Database\Eloquent\Model;

class BulletinBoardAd extends Model
{
    protected $fillable = ['title','ad', 'category_id'];
}
