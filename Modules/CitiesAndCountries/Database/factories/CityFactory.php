<?php

use Faker\Generator as Faker;

$factory->define(\Modules\CitiesAndCountries\Entities\City::class, function (Faker $faker) {
    $cons = \Modules\CitiesAndCountries\Entities\Country::all(['id']);
    $ids = [];
    foreach ($cons as $con) {
        $ids[] = $con->id;
    }


    return [
        'country_id'=>$ids[array_rand($ids)],
        'name'=>$faker->city
    ];
});
