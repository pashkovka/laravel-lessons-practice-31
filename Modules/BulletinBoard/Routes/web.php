<?php

Route::prefix('bulletinboard')->group(function() {
    Route::get('/', 'BulletinBoardController@index');
    Route::get('/show/{id}', 'BulletinBoardController@show')->name('bulletinboard.show');
    Route::post('/store', 'BulletinBoardController@store')->name('bulletinboard.store');
});
