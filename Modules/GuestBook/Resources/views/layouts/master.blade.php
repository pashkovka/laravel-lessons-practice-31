<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Module GuestBook</title>

    {{-- Laravel Mix - CSS File --}}
    {{-- <link rel="stylesheet" href="{{ mix('css/guestbook.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <style>
        form{
            margin-bottom: 100px;
        }
    </style>


</head>
<body>

@if(Session::has('success'))
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p style="color: green" class="text-center h3">{{ Session::get('success') }}</p>
            </div>
        </div>
    </div>
@endif

@include('menu_modules')

@yield('content')

{{-- Laravel Mix - JS File --}}
{{-- <script src="{{ mix('js/guestbook.js') }}"></script> --}}
</body>
</html>
