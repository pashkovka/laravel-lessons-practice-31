@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Достопримечательность</h1>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.place.update', $place->id) }}" method="post">
                @csrf
                <p><input type="text" value="{{ $place->name }}" name="name" size="150"></p>
                <p><textarea name="description" cols="150" rows="10">{{ $place->description }}</textarea></p>
                <p><input type="submit" value="Сохранить"></p>
            </form>
        </div>
    </div>
@stop