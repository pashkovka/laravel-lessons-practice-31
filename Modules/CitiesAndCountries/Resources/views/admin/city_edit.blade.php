@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Город</h1>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.city.update', $city->id) }}" method="post">
                @csrf
                <p><input type="text" value="{{ $city->name }}" name="name"></p>
                <p><input type="submit" value="Сохранить"></p>
            </form>
        </div>
    </div>
@stop