@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Страны</h1>
    <p><a href="{{ route('admin.country.create') }}">Добавить страну</a></p>
<table class="table table-hover">
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>CREATED</th>
        <th>UPDATED</th>
        <th>#</th>
        <th>#</th>
        <th>#</th>
    </tr>
    @foreach($countries as $country)
        <tr>
            <td>{{ $country->id }}</td>
            <td><a href="{{ route('admin.cities.edit', $country->id) }}">{{ $country->name }}</a></td>
            <td>{{ $country->created_at }}</td>
            <td>{{ $country->updated_at }}</td>
            <td><a href="{{ route('admin.city.create', $country->id) }}">Добавить город</a></td>
            <td><a href="{{ route('admin.country.edit', $country->id) }}">Редактировать</a></td>
            <td><a href="{{ route('admin.country.destroy', $country->id) }}" onclick="return confirm('Удалить все вложенные элементы?')">Удалить</a></td>
        </tr>
    @endforeach
</table>
@stop