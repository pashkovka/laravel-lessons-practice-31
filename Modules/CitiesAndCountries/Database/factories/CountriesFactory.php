<?php

use Faker\Generator as Faker;

$factory->define(\Modules\CitiesAndCountries\Entities\Country::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->country
    ];
});
