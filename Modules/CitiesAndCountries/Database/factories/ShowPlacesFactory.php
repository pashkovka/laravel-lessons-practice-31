<?php

use Faker\Generator as Faker;

$factory->define(\Modules\CitiesAndCountries\Entities\ShowPlace::class, function (Faker $faker) {
    $cites = \Modules\CitiesAndCountries\Entities\City::all(['id']);
    $ids = [];
    foreach ($cites as $city) {
        $ids[] = $city->id;
    }

    return [
      'city_id' => $ids[array_rand($ids)],
      'name' => 'Достопримечательность - '.implode(' ', $faker->words(2)),
      'description' => 'Достопримечательность - '.$faker->realText(300)
    ];
});
