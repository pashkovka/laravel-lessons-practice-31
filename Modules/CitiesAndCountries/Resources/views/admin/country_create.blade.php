@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Добавить страну</h1>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.country.store') }}" method="post">
                @csrf
                <p><input type="text" name="name" size="150"></p>
                <p><input type="submit" value="Сохранить"></p>
            </form>
        </div>
    </div>
@stop