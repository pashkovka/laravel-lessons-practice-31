<?php

use Faker\Generator as Faker;

$factory->define(\Modules\BulletinBoard\Entities\BulletinBoardAd::class, function (Faker $faker) {
    $cats = \Modules\BulletinBoard\Entities\BulletinBoardCategory::all(['id']);
    $ids = [];
    foreach ($cats as $cat){
        $ids[] = $cat->id;
    }
    $time = $faker->dateTime('now', 'Europe/Moscow');

    return [
        'category_id' => $ids[array_rand($ids)],
        'title' => implode(' ', $faker->words(5)),
        'ad' => implode(' ', $faker->paragraphs()),
        'created_at'=>$time,
        'updated_at'=>$time
    ];
});
