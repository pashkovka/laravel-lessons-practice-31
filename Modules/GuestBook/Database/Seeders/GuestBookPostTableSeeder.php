<?php

namespace Modules\GuestBook\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\GuestBook\Entities\GuestBookPost;

class GuestBookPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(GuestBookPost::class, 5)->create();
    }
}
