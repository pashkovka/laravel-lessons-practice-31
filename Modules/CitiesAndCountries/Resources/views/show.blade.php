@extends('citiesandcountries::layouts.master')

@section('content')
    <div class="card mb-2">
        <div class="card-title">
            <h3>{{ $place->name }}</h3>
        </div>
        <div class="card-body">
            {{ $place->description }}
        </div>
    </div>
@endsection