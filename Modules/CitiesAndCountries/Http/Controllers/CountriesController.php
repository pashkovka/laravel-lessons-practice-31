<?php

namespace Modules\CitiesAndCountries\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CitiesAndCountries\Entities\Country;

class CountriesController extends Controller
{
    public function index()
    {
        $countries = Country::all();

        return view('citiesandcountries::index', compact('countries'));
    }
}
