<?php

namespace Modules\CitiesAndCountries\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class CitiesAndCountriesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CountriesSeederTableSeeder::class);
        $this->call(CitySeederTableSeeder::class);
        $this->call(ShowPlacesTableSeeder::class);
    }
}
