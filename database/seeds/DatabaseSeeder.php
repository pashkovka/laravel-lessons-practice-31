<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserTableSeeder::class);

         $this->call(\Modules\GuestBook\Database\Seeders\GuestBookDatabaseSeeder::class);
         $this->call(\Modules\BulletinBoard\Database\Seeders\BulletinBoardDatabaseSeeder::class);
         $this->call(\Modules\CitiesAndCountries\Database\Seeders\CitiesAndCountriesDatabaseSeeder::class);
    }
}
