@extends('citiesandcountries::layouts.master')

@section('content')

    <div class="card">
        <div class="card-body">
            @foreach($city->places as $place)
                <h4>{{ $place->name }}</h4>
            @endforeach
        </div>
    </div>

    <h1>Добавить достопримечательность</h1>
    <div class="card">
        <div class="card-title">
            <p>Город: <b>{{ $city->name }}</b></p>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.place.store') }}" method="post">
                @csrf
                <input type="hidden" name="city_id" value="{{ $city->id }}">
                <p><input type="text" name="name" size="150"></p>
                <p><textarea name="description" cols="150" rows="10"></textarea></p>
                <p><input type="submit" value="Сохранить"></p>
            </form>
        </div>
    </div>
@stop
{{--todo--}}