<?php

namespace Modules\CitiesAndCountries\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CitiesAndCountries\Entities\City;

class CityController extends Controller
{
    public function index($id)
    {
        $cities = City::all()->where('country_id', $id);
        return view('citiesandcountries::cities', compact('cities'));
    }
}
