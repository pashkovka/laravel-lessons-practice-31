@extends('bulletinboard::layouts.master')

@section('content')
    @foreach($bulletins as $bulletin)

        {{--{!! dump($bulletin); die() !!}--}}

        <div class="card" style="margin-bottom: 20px">
            <div class="card-title">
                <h2 style="padding: 5px 10px;" class="text-capitalize">{{ $bulletin->title }}</h2>
                <p>Создано: {{ $bulletin->created_at }}</p>
            </div>
            <div class="card-body">
                {{ $bulletin->ad }}
            </div>
        </div>
    @endforeach

    <form action="{{ route('bulletinboard.store') }}" method="post">
        @csrf
        <input type="hidden" name="category_id" value="{{ $bulletin->category_id }}">
        <p><input type="text" name="title" size="100" placeholder="Заголовок"></p>
        <p><textarea name="ad" cols="100" rows="10" placeholder="Сообщение"></textarea></p>
        <input type="submit">
    </form>
@endsection