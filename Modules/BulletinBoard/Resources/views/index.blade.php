@extends('bulletinboard::layouts.master')

@section('content')
    <h1>Рубрики</h1>

    @foreach($cats as $cat)
        <p><a href="{{ route('bulletinboard.show', $cat->id) }}">{{ $cat->name }}</a></p>
    @endforeach
@stop
