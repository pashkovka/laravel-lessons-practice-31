<?php

namespace Modules\BulletinBoard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\BulletinBoard\Entities\BulletinBoardAd;
use Modules\BulletinBoard\Entities\BulletinBoardCategory;

class BulletinBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cats = BulletinBoardCategory::all()->sortByDesc('created_at');

        return view('bulletinboard::index', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bulletinboard::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        BulletinBoardAd::create([
            'title'=>$request->title,
            'ad'=>$request->ad,
            'category_id'=>$request->category_id
        ]);

        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $bulletins = BulletinBoardAd::all()->where('category_id', $id)->sortByDesc('created_at');
        return view('bulletinboard::show', compact('bulletins'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('bulletinboard::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
