<div class="container">
    <nav class="navbar navbar-light bg-light">
        <ul class="navbar flex-row align-content-between" style="list-style: none">

            <li class="nav-item">
                <a class="nav-link" href="/guestbook">Гостевая книга</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/guestbook/admin">Админка: Гостевая книга</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bulletinboard">Доска объявлений</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/citiesandcountries">Города и страны</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/citiesandcountries/admin">Админка: Города и страны</a>
            </li>

        </ul>
    </nav>
</div>
