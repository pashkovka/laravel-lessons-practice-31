<?php

namespace Modules\BulletinBoard\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BulletinBoardDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(BulletinBoardCategoriesSeederTableSeeder::class);
         $this->call(BulletinBoardAdSeederTableSeeder::class);
    }
}
