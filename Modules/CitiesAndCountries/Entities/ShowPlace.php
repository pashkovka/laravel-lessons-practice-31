<?php

namespace Modules\CitiesAndCountries\Entities;

use Illuminate\Database\Eloquent\Model;

class ShowPlace extends Model
{
    protected $fillable = ['name', 'description', 'city_id'];
}
