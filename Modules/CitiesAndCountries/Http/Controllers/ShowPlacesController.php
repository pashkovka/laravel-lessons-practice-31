<?php

namespace Modules\CitiesAndCountries\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CitiesAndCountries\Entities\ShowPlace;

class ShowPlacesController extends Controller
{
    public function index($id)
    {
        $places = ShowPlace::all()->where('city_id', $id);
        return view('citiesandcountries::places', compact('places'));
    }


    public function show($id)
    {
        $place = ShowPlace::findOrFail($id);
        return view('citiesandcountries::show', compact('place'));
    }
}
