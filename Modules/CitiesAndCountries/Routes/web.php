<?php


Route::prefix('citiesandcountries')->group(function() {
    Route::get('/', 'CountriesController@index')->name('countries');
    Route::get('/city/{id}', 'CityController@index')->name('city');
    Route::get('/places/{id}', 'ShowPlacesController@index')->name('places');
    Route::get('/place/{id}', 'ShowPlacesController@show')->name('place');

    Route::prefix('admin')->group(function (){
        Route::get('/', 'AdminCountryController@index')->name('admin.country.list');

        Route::prefix('country')->group(function (){
            Route::get('/show/cities/{id}', 'AdminCountryController@show')->name('admin.cities.edit');
            Route::get('/edit/{id}', 'AdminCountryController@edit')->name('admin.country.edit');
            Route::post('/update/{id}', 'AdminCountryController@update')->name('admin.country.update');
            Route::get('/destroy/{id}', 'AdminCountryController@destroy')->name('admin.country.destroy');
            Route::get('/create', 'AdminCountryController@create')->name('admin.country.create');
            Route::post('/store', 'AdminCountryController@store')->name('admin.country.store');
        });

        Route::prefix('city')->group(function (){
            Route::get('/show/places/{id}', 'AdminCitiesController@show')->name('admin.places.show');
            Route::get('/edit/{id}', 'AdminCitiesController@edit')->name('admin.city.edit');
            Route::post('/update/{id}', 'AdminCitiesController@update')->name('admin.city.update');
            Route::get('/destroy/{id}', 'AdminCitiesController@destroy')->name('admin.city.destroy');
            Route::get('/create/{id}', 'AdminCitiesController@create')->name('admin.city.create');
            Route::post('/store', 'AdminCitiesController@store')->name('admin.city.store');
        });

        Route::prefix('place')->group(function (){
            Route::get('/edit/{id}', 'AdminPlacesController@edit')->name('admin.place.edit');
            Route::post('/update/{id}', 'AdminPlacesController@update')->name('admin.place.update');
            Route::get('/destroy/{id}', 'AdminPlacesController@destroy')->name('admin.place.destroy');
            Route::get('/create/{id}', 'AdminPlacesController@create')->name('admin.place.create');
            Route::post('/store', 'AdminPlacesController@store')->name('admin.place.store');
        });
    });
});
