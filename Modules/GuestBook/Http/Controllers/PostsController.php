<?php

namespace Modules\GuestBook\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GuestBook\Entities\GuestBookPost;

class PostsController extends GuestBookController
{

    public function index()
    {
        $posts = GuestBookPost::with('user')->get();

        return view('guestbook::index', compact('posts'));
    }



    public function store(Request $request)
    {
        $user = new User();
        $user_id = $user->insertGetId([
            'name'=>$request->user
        ]);

        $post = new GuestBookPost();
        $post->title = $request->title;
        $post->post = $request->post;
        $post->user_id = $user_id;
        $post->save();

        $request->session()->flash('success', 'Сообщение сохранено');

        return back();
    }

}
