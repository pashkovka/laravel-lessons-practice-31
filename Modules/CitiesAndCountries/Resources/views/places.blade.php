@extends('citiesandcountries::layouts.master')

@section('content')
    <h1>Достопримечательности</h1>

    @foreach($places as $place)
        <div class="card mb-2">
            <div class="card-title">
                <h3><a href="{{ route('place', $place->id) }}">{{ $place->name }}</a></h3>
            </div>
        </div>
    @endforeach
@endsection