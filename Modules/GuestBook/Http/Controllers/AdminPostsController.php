<?php

namespace Modules\GuestBook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GuestBook\Entities\GuestBookPost;

class AdminPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $posts = GuestBookPost::with('user')->get();

        return view('guestbook::admin_index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('guestbook::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('guestbook::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $post = GuestBookPost::findOrFail($id);

        return view('guestbook::edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        \DB::table('guest_book_posts')
            ->where('id', $id)
        ->update([
            'title' => $request->title,
            'post' => $request->post
        ]);
        session()->flash('success', 'Обновлено.');

        if ($request->has('self')){
            return back();
        }
        return redirect()->route('guestbook.post.all');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        \DB::table('guest_book_posts')
            ->delete($id);
        session()->flash('success', 'Удалено.');

        return back();
    }
}
