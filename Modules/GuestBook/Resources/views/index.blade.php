@extends('guestbook::layouts.master')

@section('content')
    <div class="container">
        @foreach($posts as $post)
            <div class="card">
                <div class="card-title">
                    <h2>{{ $post->title }}</h2>
                </div>
                <div class="card-body">
                    <span class="date">{{ $post->created_at }}</span>
                    <p>{{ $post->user->name }}</p>
                    {{ $post->post }}
                </div>
            </div>
            <hr>
        @endforeach

        <form action="/guestbook/post/add" method="post">
            @csrf
            <p><input type="text" name="title" size="100" placeholder="Заголовок"></p>
            <p><input type="text" name="user" size="100" placeholder="Ваше имя"></p>
            <textarea name="post" cols="100" rows="10" placeholder="Сообщение"></textarea>
            <p><input type="submit" value="Go!"></p>
        </form>
    </div>

@stop
